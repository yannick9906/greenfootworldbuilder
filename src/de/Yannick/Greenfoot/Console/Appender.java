package de.Yannick.Greenfoot.Console;

public class Appender {
	public final int LEVEL_ERR = 1;
	public final int LEVEL_NOR = 0;
	public final String prefix;
	
	public Appender(String Prefix) {
		this.prefix = Prefix;
		System.err.println("[NeXT-Console]: Started");
		System.err.println("[NeXT-Console]: Prefix set to \"" + prefix + "\"");
	}
	
	public void append(String text, int Level) {
		if(Level == LEVEL_ERR) {
			System.err.println(prefix + " " + text);
		}
		else if(Level == LEVEL_NOR) {
			System.out.println(prefix + " " + text);
		}
	}
	
	public void append() {
		System.out.println();
	}
}
