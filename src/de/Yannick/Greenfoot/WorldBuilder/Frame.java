package de.Yannick.Greenfoot.WorldBuilder;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.*;

import de.Yannick.Greenfoot.Console.Appender;

public final class Frame extends JFrame implements ClipboardOwner{

	private Toolkit t;
	private int x = 0, y = 0, width = 325, height = 290;
	private Appender a;
	public static final String VERSION = "v2 Build 8";
	String holeDataString = new String(); //String der dann in Greenfoot kommt

	//Buttons
	public JButton BtnImport, BtnExport;
	public JLabel LblCopyright;
	public JButton[][] BtnGrid;
	public boolean[][] BtnGridState = new boolean[14][10];
	public JTextArea textarea1, textarea2;
	public boolean Pasted = false;
	public JFrame f1;

	public Frame() {
		t = Toolkit.getDefaultToolkit();

		Dimension d = t.getScreenSize();
		x = (int) ((d.getWidth() -  width) / 2);
		y = (int) ((d.getHeight() - height) / 2);

		a = new Appender("[WorldBuilder]:");

		setTitle("GF World Builder " + VERSION);
		setBounds(x, y, width, height);
		setLayout(null);
		setResizable(false);

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		a.append("init Componets...", a.LEVEL_NOR);
		this.initComponents();
		a.append("init Listeners...", a.LEVEL_NOR);
		this.initListeners();
		a.append("Done.", a.LEVEL_NOR);
		a.append("Started", a.LEVEL_NOR);

		setVisible(true);
	}

	private void initListeners() {

		a.append("Listeners: BtnGrid...", a.LEVEL_NOR);
		for(int i = 0; i < BtnGrid.length; i++) {
			for(int j = 0; j < BtnGrid[i].length; j++) {
				this.BtnGrid[i][j].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String text = ((JButton) e.getSource()).getText();
						String[] chars = text.split("-");
						int i = Integer.parseInt(chars[0]);
						int j = Integer.parseInt(chars[1]);
						if(BtnGridState[i][j] == false) {
							BtnGrid[i][j].setBackground(Color.BLUE);
							BtnGrid[i][j].setForeground(Color.BLUE);
							BtnGridState[i][j] = true;
						} else {
							BtnGrid[i][j].setBackground(Color.BLACK);
							BtnGrid[i][j].setForeground(Color.BLACK);
							BtnGridState[i][j] = false;
						}
					}
				});
			}
		}

		a.append("Listeners: Export Button...", a.LEVEL_NOR);
		this.BtnExport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				a.append("Export Button called...", a.LEVEL_NOR);

				holeDataString = "";
				String[] rows = new String[BtnGridState.length]; //Array mit allen Reihen des Feldes
				a.append("Exporting ....", a.LEVEL_NOR);
				for (int i = 0; i < BtnGridState.length; i++) { //Diesen dann abarbeiten
					String[] data = new String[BtnGridState[i].length]; //Array mit allen einzelnen Feldern in jeder Reihe
					for (int j = 0; j < BtnGridState[i].length; j++) { //Den auch abarbeiten
						data[j] = "Wände[" + i + "][" + j + "] = " + BtnGridState[i][j] + "; "; //Und nun das felde mit dem akt. Status bestücken
					}

					rows[i] = ""; //Damit wir kein null am Anfang jeder Reihe haben

					for (int j = 0; j < data.length; j++) {
						rows[i] = rows[i] + data[j]; //Und hier dann die Daten in Reihen zusammenbauen
					}
				}

				for (int j = 0; j < rows.length; j++) {
					holeDataString = holeDataString + rows[j] + "\n"; // Die Reihen zur fetigen ausgabe zusammenbauen
				}
				a.append("Exporting complete....", a.LEVEL_NOR);

				//System.out.println(holeDataString);//Debug

				a.append("Opening Output Frame ....", a.LEVEL_NOR);
				/*Ausgabefenster*/
				JFrame f = new JFrame();
				f.setLayout(null);

				//Elemente
				textarea1 = new JTextArea();
				textarea1.setText(holeDataString);
				JButton btncopy = new JButton("Kopieren");
				textarea1.setBounds(0, 0, 600, 400);
				btncopy.setBounds(0, 342, 100, 20);
				textarea1.setEditable(false);

				//Hinzufügen
				f.getContentPane().add(btncopy);
				f.getContentPane().add(textarea1);

				btncopy.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						a.append("Copied!!", a.LEVEL_NOR);
						setClipboardContents(holeDataString);
					}
				});

				//Fertig
				f.setTitle("Export - Greenfoot World Builder " + VERSION);
				f.setBounds(getBounds().x, getBounds().y, 600, 400);
				f.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				f.setVisible(true);
				a.append("Finished\n\n\n\n\n\n", a.LEVEL_NOR);
			}
		});

		a.append("Listeners: Import Button...", a.LEVEL_NOR);
		this.BtnImport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				a.append("Importing called...", a.LEVEL_NOR);
				a.append("Opening Frame...", a.LEVEL_NOR);
				/*Eingabefenster*/
				f1 = new JFrame();
				f1.setLayout(null);

				//Elemente
				textarea2 = new JTextArea();
				JButton btnpaste = new JButton("Einfügen");
				textarea2.setBounds(0, 0, 600, 400);
				btnpaste.setBounds(0, 342, 100, 20);
				textarea2.setEditable(false);
				textarea2.setText("Bitte den Text in die Zwischenablage legen\nund auf Kopieren klicken");
				Pasted = false;

				//Hinzufügen
				f1.getContentPane().add(btnpaste);
				f1.getContentPane().add(textarea2);
				a.append("Frame opened.", a.LEVEL_NOR);

				btnpaste.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						a.append("Pasted.", a.LEVEL_NOR);
						holeDataString = getClipboardContents();
						textarea2.setText(holeDataString);
						Pasted = true;
						a.append("Data recieved.", a.LEVEL_NOR);
						processInput(holeDataString);
						f1.dispose();
						a.append("Frame closed.", a.LEVEL_NOR);
						a.append("Import finished.\n\n\n\n\n\n", a.LEVEL_NOR);
					}
				});

				//Fertig
				f1.setTitle("Import - Greenfoot World Builder " + VERSION);
				f1.setBounds(getBounds().x, getBounds().y, 600, 400);
				f1.setVisible(true);
				a.append("Waiting for Data....", a.LEVEL_NOR);

			}
		});

	}

	private void processInput(String holeDataString) {
		a.append("Input sawn :D ....", a.LEVEL_NOR);
		String[] lines = holeDataString.split("\n");

		a.append("Processing", a.LEVEL_NOR);
		for(int i = 0; i < lines.length; i++) {
			String[] onePart = lines[i].split(";");
			for(int j = 0; j < onePart.length - 1;j++) {
				onePart[j] = onePart[j].replace("Wände["+i+"]["+j+"] = ", "");
				onePart[j] = onePart[j].replace("; ", "");
				onePart[j] = onePart[j].replace(" ", "");
				//System.out.println(onePart[j]);
				boolean output = Boolean.parseBoolean(onePart[j]);
				try {
					BtnGridState[i][j] = output;
					//System.out.print(i+";"+j+": "+output);
				} catch (ArrayIndexOutOfBoundsException e) {
					a.append("Error at: i:"+ i + "; J:" + j + "!!", a.LEVEL_ERR);
				}
			}
			System.out.print("\r Processing Line " + (i+1) + " of " + lines.length);

		}
		a.append("\nProcessing Complete", a.LEVEL_NOR);

		a.append("Reload Buttons ....", a.LEVEL_NOR);
		reloadBtnWithStateArray();
	}

	private void reloadBtnWithStateArray() {

		a.append("Reloading Buttons: called", a.LEVEL_NOR);
		for(int i = 0; i < BtnGridState.length; i++) {
			for(int j = 0; j < BtnGridState[i].length; j++) {
				if(BtnGridState[i][j] == true) {
					BtnGrid[i][j].setBackground(Color.BLUE);
					BtnGrid[i][j].setForeground(Color.BLUE);
				} else {
					BtnGrid[i][j].setBackground(Color.BLACK);
					BtnGrid[i][j].setForeground(Color.BLACK);
				}
			}
		}
		a.append("Reloading Buttons: finished", a.LEVEL_NOR);
	}

	private void initComponents() {

		a.append("Comp: Construct", a.LEVEL_NOR);
		//Zuweisen
		this.BtnExport = new JButton("⊽ Export");
		this.BtnImport = new JButton("⊼ Import");
		this.LblCopyright 	= new JLabel("(C) Yannick Félix");

		a.append("Comp: BtnGrid", a.LEVEL_NOR);
		//GridButtons abarbeiten
		this.BtnGrid = new JButton[14][10];
		for(int i = 0; i < BtnGrid.length; i++) {
			for(int j = 0; j < BtnGrid[i].length; j++) {
				BtnGrid[i][j] = new JButton(i+"-"+j); //Zuweisen
				BtnGrid[i][j].setBackground(Color.BLACK); //Färben
				BtnGrid[i][j].setBounds(i*22+5,j*22+5,20,20);//Positionieren
				getContentPane().add(BtnGrid[i][j]);//Hinzufägen
			}
		}

		a.append("Comp: Position", a.LEVEL_NOR);
		//Position
		this.BtnExport.setBounds(5, 235, 95, 20);
		this.BtnImport.setBounds(110, 235, 95, 20);
		//this.BtnImport.setEnabled(false);
		this.LblCopyright.setBounds(210, 235, 100, 20);

		a.append("Comp: Add", a.LEVEL_NOR);
		//Hinzufügen
		getContentPane().add(BtnExport);
		getContentPane().add(BtnImport);
		getContentPane().add(LblCopyright);

		a.append("Comp: Finished", a.LEVEL_NOR);
	}

	/**
	 *  Empty implementation of the ClipboardOwner interface.
	 */
	@Override public void lostOwnership(Clipboard aClipboard, Transferable aContents){
		//do nothing
	}

	/**
	 * Place a String on the clipboard, and make this class the
	 * owner of the Clipboard's contents.
	 */
	public void setClipboardContents(String aString){
		StringSelection stringSelection = new StringSelection(aString);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, this);
	}

	/**
	 * Get the String residing on the clipboard.
	 *
	 * @return any text found on the Clipboard; if none found, return an
	 * empty String.
	 */
	public String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		//odd: the Object param of getContents is not currently used
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText =
				(contents != null) &&
						contents.isDataFlavorSupported(DataFlavor.stringFlavor)
				;
		if (hasTransferableText) {
			try {
				result = (String)contents.getTransferData(DataFlavor.stringFlavor);
			}
			catch (UnsupportedFlavorException | IOException ex){
				System.out.println(ex);
				ex.printStackTrace();
			}
		}
		return result;
	}
}
